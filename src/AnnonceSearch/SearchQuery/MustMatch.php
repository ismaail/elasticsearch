<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class MustMatch
 * @package Ismaail\Elasticsearch\AnnonceSearch
 */
class MustMatch implements SearchQueryInterface
{
    /**
     * @inheritDoc
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        $qb->mustMatch($fields[0], $value);
    }
}
