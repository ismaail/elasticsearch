<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class QueryString
 * @package Ismaail\Elasticsearch\AnnonceSearch\SearchQuery
 */
class QueryString implements SearchQueryInterface
{
    /**
     * @inheritDoc
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        /** @var \App\Models\Category|null $category */
        $category = \Search::get('category');

        if (null !== $category) {
            $fields = array_merge($fields, $this->getCategorySearchFields($category->id));
        }

        $qb->queryString($value, $fields);
    }

    /**
     * Get Category Search Fields and add the prefix 'fields." to each found field.
     *
     * @param int $id Category ID
     *
     * @return array
     */
    private function getCategorySearchFields(int $id)
    {
        return array_map(function (string $value) {
            return sprintf('fields.%s', $value);
        }, \CategoryRepository::getSearchFields($id));
    }
}
