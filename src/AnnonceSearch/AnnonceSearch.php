<?php

namespace Ismaail\Elasticsearch\AnnonceSearch;

use Ismaail\Elasticsearch\QueryBuilder;
use Ismaail\Elasticsearch\ElasticsearchException;

/**
 * Class AnnonceSearch
 * @package Ismaail\Elasticsearch\AnnonceSearch
 */
class AnnonceSearch
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * AnnonceSearch constructor.
     */
    public function __construct()
    {
        $this->config = config('annonces.elasticsearch.search.mappings');
    }

    /**
     * @param QueryBuilder $qb
     * @param array $input
     *
     * @throws ElasticsearchException
     */
    public function makeSearchQuery(QueryBuilder $qb, array $input)
    {
        foreach ($input as $name => $value) {
            $this->search($qb, $name, $value);
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param string $name
     * @param string $value
     *
     * @throws ElasticsearchException
     */
    private function search(QueryBuilder $qb, $name, $value)
    {
        $searchType = $this->config[$name] ?? null;

        if (null === $searchType) {
            throw new ElasticsearchException(sprintf('Search Mapping "%s" not found.', $name));
        }

        $classname = sprintf(
            '\Ismaail\Elasticsearch\AnnonceSearch\SearchQuery\%s',
            toStudlyCaps($searchType['query'])
        );

        if (! class_exists($classname)) {
            throw new ElasticsearchException(
                sprintf('Unsupported Search Mapping "%s for input "%s".', $searchType['query'], $name)
            );
        }

        // If the value is an object,
        // get the correct value from the property of this object
        if (\is_object($value)) {
            $value = $value->{$searchType['property']};
        }

        /** @var SearchQuery\SearchQueryInterface $searchQuery */
        $searchQuery = new $classname();
        $searchQuery->make($qb, $this->config[$name]['fields'], $value);
    }
}
