<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Class Price
 *
 * @package Ismaail\Elasticsearch\Mapper
 */
class Price implements MapperInterface
{
    /**
     * @var float|null
     */
    public $amount;

    /**
     * @var string|null
     */
    public $note;

    /**
     * @param array $data
     */
    public function fill(array $data)
    {
        $this->amount = $data['amount'];
        $this->note = $data['note'];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
