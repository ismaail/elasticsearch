<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Class Category
 * @package Ismaail\Elasticsearch\Mapper
 *
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class Category implements MapperInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $public_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var Category
     */
    public $parent;

    /**
     * @var Category
     */
    public $root;

    /**
     * @param int $id
     *
     * @throws MapperException
     */
    public function map(int $id)
    {
        /** @var \App\Models\Category $category */
        $category = \App\Models\Category
            ::with('parent')
            ->where(['id' => $id])
            ->first();

        if (null === $category) {
            throw new MapperException("Category not found for id {$id}");
        }

        $this->id = $category->id;
        $this->public_id = $category->public_id;
        $this->name = $category->name;
        $this->slug = $category->slug;

        $this->parent = [
            'id' => $category->parent->id,
            'public_id' => $category->parent->public_id,
            'name' => $category->parent->name,
            'slug' => $category->parent->slug,
        ];

        $root = $category->parent;

        if (null !== $root['parent_id']) {
            $root = $root->parent;
        }

        $this->root = [
            'id' => $root->id,
            'public_id' => $root->public_id,
            'name' => $root->name,
            'slug' => $root->slug,
        ];
    }

    /**
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fill(array $data)
    {
        throw new MapperException('Not dogin any thing.');
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
