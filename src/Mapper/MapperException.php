<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Class MapperException
 *
 * @package Ismaail\Elasticsearch\Mapper
 */
class MapperException extends \Exception
{
}
