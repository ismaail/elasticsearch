<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class Exclude
 *
 * @package Ismaail\Elasticsearch\AnnonceSearch\SearchQuery
 */
class Exclude implements SearchQueryInterface
{
    /**
     * @param QueryBuilder $qb
     * @param array $fields
     * @param string|array $value
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        $qb->mustNot($fields[0], $value);
    }
}
