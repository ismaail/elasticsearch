<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Class Hit
 * @package Ismaail\Elasticsearch
 */
class Hit
{
    /**
     * @var Annonce
     */
    public $source;

    /**
     * @var string
     */
    public $id;

    /**
     * @return Annonce
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param Annonce $source
     */
    public function setSource(Annonce $source)
    {
        $this->source = $source;

        $this->source->esid = $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
