<?php

namespace Ismaail\Elasticsearch;

use Ismaail\Elasticsearch\Mapper\Hit;
use Ismaail\Elasticsearch\Mapper\Annonce;
use Ismaail\Elasticsearch\Validator\AnnonceJsonSchema;
use Ismaail\Elasticsearch\AnnonceSearch\AnnonceSearch;

/**
 * Class Elasticsearch
 * @package Ismaail\Elasticsearch
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Elasticsearch
{
    /**
     * @var string
     */
    private $index;

    /**
     * @var string
     */
    private $type;

    /**
     * Elasticsearch constructor.
     */
    public function __construct()
    {
        $this->index = config('elasticsearch.index');
        $this->type = config('elasticsearch.type');
    }

    /**
     * @param int|string $id
     * @param string $status
     * @param string $field
     *
     * @return \Ismaail\Elasticsearch\Mapper\Annonce
     *
     * @throws Validator\JsonSchemaValidatorException
     * @throws \JsonMapper_Exception
     */
    public function find($id, string $status = null, string $field = 'public_id')
    {
        $qb = new QueryBuilder($this->index, $this->type);

        $qb->mustMatch($field, $id);

        $this->prepareStatusFilter($qb, $status);

        $response = \Es::search($qb->getQuery());

        $this->validateJson($response);

        if (0 === $response['hits']['total']) {
            return null;
        }

        return $this->map($response['hits']['hits'][0]);
    }

    /**
     * @param $id
     * @param string|null $status
     *
     * @return \Ismaail\Elasticsearch\Mapper\Annonce
     *
     * @throws Validator\JsonSchemaValidatorException
     * @throws \JsonMapper_Exception
     */
    public function findBySlug($id, string $status = null)
    {
        return $this->find($id, $status, 'slug');
    }

    /**
     * Check If Annonce Exists in Elasticsearch
     *
     * @param string $publicId
     *
     * @return bool
     *
     * @throws \App\Elasticsearch\Validator\JsonSchemaValidatorException
     * @throws \JsonMapper_Exception
     */
    public function exists(string $publicId): bool
    {
        return null !== $this->find($publicId);
    }

    /**
     * @param array $query
     * @param array $sort
     * @param int $page
     * @param int $perPage
     *
     * @return \App\Elasticsearch\AnnoncePaginator
     *
     * @throws Validator\JsonSchemaValidatorException
     */
    public function search(array $query, array $sort, int $page, int $perPage)
    {
        $qb = new QueryBuilder($this->index, $this->type);

        $search = new AnnonceSearch();
        $search->makeSearchQuery($qb, $query);

        $this->setupAggregations($qb, $query);

        $qb->paginate($page, $perPage);

        if (config('annonces.elasticsearch.sort.distance') === $sort[0]) {
            $qb->sortByDistance($sort[0], explode(', ', $sort[2]), $sort[1]);
        } else {
            $qb->sort(...$sort);
        }

        if (config('debugbar.enabled')) {
            \Debugbar::log($qb->getQuery());
        }

        $response = \Es::search($qb->getQuery());

        $this->validateJson($response);

        $paginator = new AnnoncePaginator(
            $this->mapArray($response['hits']['hits']),
            $response['hits']['total'],
            $perPage,
            $page,
            [
                'aggregations' => $response['aggregations'] ?? null,
                'path' => $this->fixedFullPath(),
            ]
        );

        return $paginator;
    }

    /**
     * @param \App\Models\Annonce $annonce
     */
    public function publish(\App\Models\Annonce $annonce): void
    {
        if ($this->exists($annonce->public_id)) {
            return;
        }

        $mapper = new Annonce();
        $mapper->map($annonce);

        $this->create($mapper);
    }

    /**
     * @param \Ismaail\Elasticsearch\Mapper\Annonce $annonce
     */
    public function create(Annonce $annonce): void
    {
        \Es::create([
            'id' => $annonce->id,
            'index' => $this->index,
            'type' => $this->type,
            'body' => $annonce->toArray(),
        ]);
    }

    /**
     * @param \App\Models\Annonce $annonce
     * @param array|null $data
     */
    public function update($annonce, ?array $data = null): void
    {
        if (! $this->exists($annonce->public_id)) {
            return;
        }

        $mapper = new Annonce();
        $mapper->map($annonce);

        $body = null === $data ? $mapper->toArray() : ['doc' => $data];

        \Es::update([
            'id' => $mapper->id,
            'index' => $this->index,
            'type' => $this->type,
            'body' => $body,
        ]);
    }

    /**
     * @param $data
     *
     * @return \Ismaail\Elasticsearch\Mapper\Annonce
     *
     * @throws \JsonMapper_Exception
     */
    private function map($data): Annonce
    {
        $mapper = new \JsonMapper();
        $mapper->bStrictObjectTypeChecking = true;
        $mapper->bEnforceMapType = false;

        /** @var Hit $hit */
        $hit = $mapper->map($data, new Hit());

        return $hit->getSource();
    }

    /**
     * @param $data
     *
     * @return \Ismaail\Elasticsearch\Mapper\Annonce[]
     */
    private function mapArray($data)
    {
        $mapper = new \JsonMapper();
        $mapper->bStrictObjectTypeChecking = true;
        $mapper->bEnforceMapType = false;

        /** @var Hit[] $hits */
        $hits = $mapper->mapArray($data, [], Hit::class);

        return array_column($hits, 'source');
    }

    /**
     * @param array $response
     *
     * @throws Validator\JsonSchemaValidatorException
     */
    private function validateJson($response) :void
    {
        $validator = new AnnonceJsonSchema();
        $validator->validate($response);
    }

    /**
     * @param \App\Elasticsearch\QueryBuilder $qb
     * @param string $status
     */
    private function prepareStatusFilter(QueryBuilder $qb, string $status = null): void
    {
        if (null === $status) {
            return;
        }

        $qb->mustMatch('status', $status);
    }

    /**
     * @return string
     */
    private function fixedFullPath(): string
    {
        parse_str(parse_url(\URL::full(), PHP_URL_QUERY), $params);

        unset($params['page']);

        return sprintf(
            '%s%s%s',
            \URL::current(),
            (empty($params) ? '' : '?'),
            http_build_query($params)
        );
    }

    /**
     * @param QueryBuilder $qb
     * @param array $input
     */
    public function setupAggregations(QueryBuilder $qb, array $input): void
    {
        $qb
            ->aggregate('price_min', 'min', 'price.amount')
            ->aggregate('price_max', 'max', 'price.amount')
            ->aggregate('category', 'terms', 'category.public_id')
            ->aggregate('category_parent', 'terms', 'category.parent.public_id')
            ;

        $this->setupCategoryAggregations($qb, $input);
    }

    /**
     * @param \App\Elasticsearch\QueryBuilder $qb
     * @param array $input
     */
    private function setupCategoryAggregations(QueryBuilder $qb, array $input): void
    {
        /** @var \App\Models\Category|int|null $category */
        $category = $input['category'] ?? null;

        if (null === $category) {
            return;
        }

        $categoryId = \is_object($category) ? $category->public_id : $category;
        $categoryAggregations = \CategoryRepository::getAggregations($categoryId);

        foreach ($categoryAggregations as $aggregation) {
            $qb->aggregate(
                $aggregation['name'],
                $aggregation['type'],
                sprintf('fields.%s', $aggregation['name'])
            );
        }
    }
}
