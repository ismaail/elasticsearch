<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class GeoDistance
 * @package Ismaail\Elasticsearch\AnnonceSearch\SearchQuery
 */
class GeoDistance implements SearchQueryInterface
{
    /**
     * @inheritDoc
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        $from = $value['from'] ?? '';
        [$lat, $lon] = explode(',', $from);

        if (null === $lat || null === $lon) {
            return;
        }

        $default = 0.001;
        $distance = $value['distance'] ?? $default;
        if (0 >= $distance) {
            $distance = $default;
        }

        $qb->geoDistance($fields[0], compact('lat', 'lon'), $distance);
    }
}
