<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class Terms
 * @package Ismaail\Elasticsearch\AnnonceSearch\SearchQuery
 */
class Terms implements SearchQueryInterface
{
    /**
     * @param QueryBuilder $qb
     * @param array $fields
     * @param string|array $value
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        $qb->terms($fields[0], $value);
    }
}
