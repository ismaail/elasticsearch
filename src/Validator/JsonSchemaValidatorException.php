<?php

namespace Ismaail\Elasticsearch\Validator;

use Exception;

/**
 * Class JsonSchemaValidatorException
 * @package Ismaail\Elasticsearch\Validator
 */
class JsonSchemaValidatorException extends Exception
{
    /**
     * @var array
     */
    private $errors;

    /**
     * JsonSchemaValidatorException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Exception $previous
     * @param array $errors
     */
    public function __construct($message, $code, Exception $previous = null, array $errors = [])
    {
        parent::__construct($message, $code, $previous);

        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
