<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Interface SearchQueryInterface
 * @package Ismaail\Elasticsearch\AnnonceSearch
 */
interface SearchQueryInterface
{
    /**
     * @param QueryBuilder $qb
     * @param array $fields
     * @param string|array $value
     */
    public function make(QueryBuilder $qb, array $fields, $value);
}
