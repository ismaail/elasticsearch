<?php

namespace Ismaail\Elasticsearch;

use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class AnnoncePaginator
 * @package Ismaail\Elasticsearch
 */
class AnnoncePaginator extends LengthAwarePaginator
{
    /**
     * @var array
     */
    public $aggregations = [];

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        $array = parent::toArray();

        $array['aggregations'] = $this->aggregations;

        return $array;
    }
}
