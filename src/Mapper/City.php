<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Class City
 * @package Ismaail\Elasticsearch\Mapper
 *
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class City implements MapperInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $public_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $slug;

    /**
     * @param array $data
     */
    public function fill(array $data)
    {
        $this->id = $data['id'];
        $this->public_id = $data['public_id'];
        $this->name = $data['name'];
        $this->slug = $data['slug'];
    }
}
