<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Class Location
 * @package Ismaail\Elasticsearch\Mapper
 */
class Location implements MapperInterface
{
    /**
     * @var City
     */
    public $city;

    /**
     * @var string|null
     */
    public $zipcode;

    /**
     * @var string|null
     */
    public $address;

    /**
     * @var array
     */
    public $geo;

    /**
     * Location constructor.
     */
    public function __construct()
    {
        $this->city = new City();
    }

    /**
     * @param array $data
     */
    public function fill(array $data)
    {
        $this->city->fill($data['city']);
        $this->zipcode = $data['zipcode'];
        $this->address = $data['address'];
        $this->geo = $data['geo'];
    }
}
