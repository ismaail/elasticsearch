<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class Range
 * @package Ismaail\Elasticsearch\AnnonceSearch\SearchQuery
 */
class Range implements SearchQueryInterface
{
    /**
     * @inheritDoc
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        $params = [];
        $min = $value['min'] ?? null;
        $max = $value['max'] ?? null;

        if ($min) {
            $params['gte'] = $min;
        }

        if ($max) {
            $params['lte'] = $max;
        }

        if (empty($params)) {
            return;
        }

        $qb->range($fields[0], $params);
    }
}
