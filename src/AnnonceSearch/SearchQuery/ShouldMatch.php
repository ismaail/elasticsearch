<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class ShouldMatch
 * @package Ismaail\Elasticsearch\AnnonceSearch\SearchQuery
 */
class ShouldMatch implements SearchQueryInterface
{
    /**
     * @param QueryBuilder $qb
     * @param array $fields
     * @param string|array $value
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        array_map(function ($field) use ($qb, $value) {
            if (! is_array($value)) {
                $value = [$value];
            }

            $qb->shouldMatch($field, $value);
        }, $fields);
    }
}
