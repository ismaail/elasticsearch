<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class MultiMatchSearchQuery
 * @package Ismaail\Elasticsearch\AnnonceSearch
 */
class MultiMatch implements SearchQueryInterface
{
    /**
     * @inheritDoc
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        $qb->multiMatch($fields, $value);
    }
}
