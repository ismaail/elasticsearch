<?php

namespace Ismaail\Elasticsearch\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Ismaail\Elasticsearch\Elasticsearch;

/**
 * Class ElasticsearchProvider
 * @package Ismaail\Elasticsearch\Providers
 */
class ElasticsearchProvider extends ServiceProvider
{
    /**
     * Register the Service Provider.
     */
    public function register()
    {
        App::bind('Elasticsearch', function () {
            return new Elasticsearch();
        });
    }
}
