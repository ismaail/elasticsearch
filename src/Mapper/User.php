<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Class User
 * @package Ismaail\Elasticsearch\Mapper
 */
class User implements MapperInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var Contact
     */
    public $contact;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->contact = new Contact();
    }

    /**
     * @param array $data
     */
    public function fill(array $data)
    {
        $this->id = $data['id'];
        $this->contact->fill($data['contact']);
    }
}
