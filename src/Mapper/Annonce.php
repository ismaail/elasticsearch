<?php

namespace Ismaail\Elasticsearch\Mapper;

use Carbon\Carbon;
use App\Models\Contact;

/**
 * Class Annonce
 * @package Ismaail\Elasticsearch
 *
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Annonce
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $public_id;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var \Carbon\Carbon
     */
    public $created_at;

    /**
     * @var \Carbon\Carbon
     */
    public $updated_at;

    /**
     * @var \Carbon\Carbon|null
     */
    public $published_at;

    /**
     * @var User
     */
    public $user;

    /**
     * @var Price
     */
    public $price;

    /**
     * @var Category
     */
    public $category;

    /**
     * @var Location
     */
    public $location;

    /**
     * @var array|null
     */
    public $fields;

    /**
     * @var array|null
     */
    public $pictures;

    /**
     * @param string $created_at
     *
     */
    public function setCreatedAt(string $created_at)
    {
        $this->created_at = new Carbon($created_at);
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt(string $updated_at)
    {
        $this->updated_at = new Carbon($updated_at);
    }

    /**
     * @param null|string $published_at
     */
    public function setPublishedAt(?string $published_at)
    {
        $this->published_at = null !== $published_at ? new Carbon($published_at) : null;
    }

    /**
     * @param \App\Models\Annonce $model
     */
    public function map(\App\Models\Annonce $model)
    {
        $this->id = $model->id;
        $this->status = $model->status;
        $this->public_id = $model->public_id;
        $this->title = $model->title;
        $this->slug = $model->slug;
        $this->description = $model->description;
        $this->created_at  = $model->created_at->toDateTimeString();
        $this->updated_at = $model->updated_at->toDateTimeString();
        $this->published_at = null !== $model->published_at ? $model->published_at->toDateTimeString() : null;
        $this->mapContact($model->contact_id);
        $this->mapPrice($model);
        $this->mapCategory($model->category_id);
        $this->mapLocation($model);
        $this->fields = $model->fields;
        $this->pictures = $model->pictures;
        //$this->stats = $model->stats;
    }

    /**
     * @param int $id Contact ID
     */
    private function mapContact(int $id)
    {
        /** @var \App\Models\Contact $contact */
        $contact = Contact
            ::where(['id' => $id])
            ->with('user')
            ->first();

        if (null === $contact) {
            throw new MapperException("Contact not found for id {$id}");
        }

        $user = new User();
        $user->fill([
            'id' => $contact->user_id,
            'contact' => $contact->getAttributes(),
        ]);

        $this->user = $user;
    }

    /**
     * Map price array to Mapper/Price Class
     *
     * @param \App\Models\Annonce
     */
    private function mapPrice(\App\Models\Annonce $annonce)
    {
        $price = new Price();

        $price->amount = $annonce->price_amount;
        $price->note = $annonce->price_note;

        $this->price = $price;
    }

    /**
     * @param int $id Category ID
     */
    private function mapCategory(int $id)
    {
        $category = new Category();
        $category->map($id);

        $this->category = $category;
    }

    /**
     * @param \App\Models\Annonce $annonce
     */
    private function mapLocation(\App\Models\Annonce $annonce)
    {
        $location = new Location();

        $location->fill([
            'city' => $annonce->city->getAttributes(),
            'address' => $annonce['address'],
            'street' => $annonce['street'],
            'zipcode' => $annonce['zipcode'],
            'geo' => [
                'lat' => $annonce['lat'],
                'lon' => $annonce['lon'],
            ],
        ]);

        $this->location = $location;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
