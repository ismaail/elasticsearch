<?php

namespace Ismaail\Elasticsearch;

/**
 * Class QueryBuilder
 * @package Ismaail\Elasticsearch
 */
class QueryBuilder
{
    /**
     * @const string
     */
    public const QUERY_OPERATOR_OR = 'or';

    /**
     * @const string
     */
    public const QUERY_OPERATOR_AND = 'and';

    /**#@+
     * @const string
     */
    public const DISTANCE_UNIT_KM = 'km';
    public const DISTANCE_UNIT_MILES = 'miles';
    /**#@- */

    /**
     * @const string
     */
    public const DISTANCE_TYPE_PLANE = 'plane';

    /**
     * @var array
     */
    private $query = [];

    /**
     * QueryBuilder constructor.
     *
     * @param string $index
     * @param string $type
     */
    public function __construct(string $index, string $type)
    {
        $this->query['index'] = $index;
        $this->query['type'] = $type;
    }

    /**
     * @param string $attribute
     * @param string $value
     *
     * @return $this
     */
    public function match(string $attribute, string $value)
    {
        $this->query['body']['query']['match'][$attribute] = $value;

        return $this;
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param string $operator
     *
     * @return $this
     */
    public function mustMatch(string $attribute, string $value, string $operator = self::QUERY_OPERATOR_AND)
    {
        $value = trim($value);

        if (empty($value)) {
            return $this;
        }

        $this->query['body']['query']['bool']['must'][]['match'][$attribute] = [
            'query' => $value,
            'operator' => $operator,
        ];

        return $this;
    }

    /**
     * @param array $fields
     * @param string $value
     * @param string $operator
     *
     * @return $this
     */
    public function multiMatch(array $fields, string $value, string $operator = self::QUERY_OPERATOR_OR)
    {
        $value = trim($value);

        if (empty($value)) {
            return $this;
        }

        $this->query['body']['query']['bool']['must'][]['multi_match'] = [
            'query' => $value,
            'fields' => $fields,
            'operator' => $operator,
        ];

        return $this;
    }

    /**
     * @param string $field
     * @param array $value
     *
     * @return \App\Elasticsearch\QueryBuilder
     */
    public function shouldMatch(string $field, array $value): self
    {
        if (empty($value)) {
            return $this;
        }

        $should = array_map(function ($val) use ($field) {
            return ['match' => [$field => $val]];
        }, $value);

        $this->query['body']['query']['bool']['should'] = array_merge(
            $this->query['body']['query']['bool']['should'] ?? [],
            $should
        );

        return $this;
    }

    /**
     * @param string $query
     * @param array $fields
     *
     * @return $this
     */
    public function queryString(string $query, array $fields)
    {
        $this->query['body']['query']['bool']['must'][]['query_string'] = [
            'query' => $query,
            'fields' => $fields,
            'lenient' => true,
            'default_operator' => self::QUERY_OPERATOR_AND,
        ];

        return $this;
    }

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return $this
     *
     * @deprecated Not working in Elasticsearch 5.5
     */
    public function filterNot(string $field, $value)
    {
        $this->query['body']['filter']['not']['term'][$field] = $value;

        return $this;
    }

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return $this
     */
    public function mustNot(string $field, $value)
    {
        $this->query['body']['query']['bool']['must_not'][]['term'] = [
            $field => $value,
        ];

        return $this;
    }

    /**
     * @param string $field
     * @param string|int $value
     *
     * @return \App\Elasticsearch\QueryBuilder
     */
    public function terms(string $field, $value): self
    {
        $this->query['body']['query']['bool']['must'][]['bool']['should']['terms'][$field] = [$value];

        return $this;
    }

    /**
     * @param string $field
     * @param array $from
     * @param float $distance
     * @param string $unit
     *
     * @return $this
     */
    public function geoDistance(string $field, array $from, float $distance, string $unit = self::DISTANCE_UNIT_KM)
    {
        $this->query['body']['query']['bool']['filter']['geo_distance'] = [
            'distance' => $distance . $unit,
            $field => $from,
        ];

        return $this;
    }

    /**
     * @param string $field
     * @param array $params
     */
    public function range(string $field, array $params)
    {
        $this->query['body']['query']['bool']['must'][]['range'][$field] = $params;
    }

    /**
     * @param int $page
     * @param int $perPage
     *
     * @return $this
     */
    public function paginate(int $page, int $perPage = 10)
    {
        $page = ($page <= 0) ? 1 : $page;
        $page = ($page > 1000) ? 1000 : $page;

        $this->query['body']['size'] = $perPage;
        $this->query['body']['from'] = (($page * $perPage) - $perPage);

        return $this;
    }

    /**
     * @param string $name
     * @param string $type
     * @param string $field
     *
     * @return $this
     */
    public function aggregate(string $name, string $type, string $field)
    {
        $this->query['body']['aggs'][$name][$type] = ['field' => $field];

        return $this;
    }

    /**
     * @param string $field
     * @param string $direction
     *
     * @return $this
     */
    public function sort(string $field, string $direction = 'asc')
    {
        $this->query['body']['sort'][$field] = ['order' => $direction];

        return $this;
    }

    /**
     * @param string $field
     * @param array $location
     * @param string $direction
     * @param string $unit
     *
     * @return $this
     */
    public function sortByDistance(
        string $field,
        array $location,
        string $direction = 'asc',
        string $unit = self::DISTANCE_UNIT_KM
    ) {
        list($lat, $lon) = $location;

        $this->query['body']['sort']['_geo_distance'] = [
            $field => compact('lat', 'lon'),
            'order' => $direction,
            'unit' => $unit,
            'distance_type' => self::DISTANCE_TYPE_PLANE,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }
}
