<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Class Contact
 * @package Ismaail\Elasticsearch\Mapper
 *
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class Contact implements MapperInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string|null
     */
    public $fullname;

    /**
     * @var string|null
     */
    public $phone;

    /**
     * @var string|null
     */
    public $phone_note;

    /**
     * @var string|null
     */
    public $email;

    /**
     * @param array $data
     */
    public function fill(array $data)
    {
        $this->id = $data['id'];
        $this->fullname = $data['fullname'];
        $this->phone = $data['phone'];
        $this->phone_note = $data['phone_note'];
        $this->email = $data['email'];
    }
}
