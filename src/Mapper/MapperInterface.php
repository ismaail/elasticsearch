<?php

namespace Ismaail\Elasticsearch\Mapper;

/**
 * Interface MapperInterface
 * @package Ismaail\Elasticsearch\Mapper
 */
interface MapperInterface
{
    /**
     * @param array $data
     */
    public function fill(array $data);
}
