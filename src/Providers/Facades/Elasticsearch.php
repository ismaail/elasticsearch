<?php

namespace Ismaail\Elasticsearch\Providers\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Elasticsearch
 * @package Ismaail\Elasticsearch\Providers\Facades
 */
class Elasticsearch extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Elasticsearch';
    }
}
