<?php

namespace Ismaail\Elasticsearch\Validator;

use JsonSchema\Validator;
use JsonSchema\Uri\UriRetriever;

/**
 * Class JsonSchemaValidator
 * @package Ismaail\Elasticsearch\Validator
 */
class AnnonceJsonSchema
{
    /**
     * @param array $data
     *
     * @throws JsonSchemaValidatorException
     */
    public function validate($data)
    {
        // Convert array to json object.
        $data = json_decode(json_encode($data, JSON_THROW_ON_ERROR), false);

        $filePath = config_path('json_schema/annonce.json');
        $retriever = new UriRetriever();
        $schema = $retriever->retrieve("file://{$filePath}");

        $validator = new Validator();
        $validator->check($data, $schema);

        if (! $validator->isValid()) {
            throw new JsonSchemaValidatorException(
                'Error validating Annonce Json schema.',
                0,
                null,
                $validator->getErrors()
            );
        }
    }
}
