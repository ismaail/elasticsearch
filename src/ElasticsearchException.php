<?php

namespace Ismaail\Elasticsearch;

/**
 * Class ElasticsearchException
 * @package Ismaail\Elasticsearch
 */
class ElasticsearchException extends \Exception
{
}
