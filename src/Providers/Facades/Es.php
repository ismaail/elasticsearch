<?php

namespace Ismaail\Elasticsearch\Providers\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Es
 * @package Ismaail\Elasticsearch\Providers\Facades
 */
class Es extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'Es';
    }
}
