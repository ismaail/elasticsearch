<?php

namespace Ismaail\Elasticsearch\AnnonceSearch\SearchQuery;

use Ismaail\Elasticsearch\QueryBuilder;

/**
 * Class Fields
 * @package Ismaail\Elasticsearch\AnnonceSearch\SearchQuery
 */
class SearchFields implements SearchQueryInterface
{
    /**
     * @inheritDoc
     */
    public function make(QueryBuilder $qb, array $fields, $value)
    {
        if (! \is_array($value)) {
            return;
        }

        if (! \Search::has('category')) {
            return;
        }

        $fields = $this->filterFields(\Search::get('category')->id, $value);

        foreach ($fields as $fieldKey => $fieldValue) {
            $qb->mustMatch("fields.{$fieldKey}", $fieldValue);
        }
    }

    /**
     * @param int $categoryId
     * @param array $value
     *
     * @return array
     */
    private function filterFields(int $categoryId, $value)
    {
        $categorySearchFields = \CategoryRepository::getSearchFields($categoryId);

        return array_intersect_key($value, array_fill_keys($categorySearchFields, null));
    }
}
