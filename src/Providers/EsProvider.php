<?php

namespace Ismaail\Elasticsearch\Providers;

use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

/**
 * Class EsProvider
 * @package Ismaail\Elasticsearch\Providers
 */
class EsProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        App::singleton('Es', function () {
            return ClientBuilder::fromConfig([
                'hosts' => config('elasticsearch.hosts'),
                'retries' => config('elasticsearch.retries'),
            ]);
        });
    }
}
