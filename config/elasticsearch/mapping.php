<?php

// Fetch Categories Aggregations
$cols = \App\Models\Category::whereNotNull('aggregations')->get()->toArray();
$aggregations = [];

array_map(function ($col) use (&$aggregations) {
    foreach ($col['aggregations'] as $aggregation) {
        if (isset($aggregations[$aggregation['name']])) {
            continue;
        }

        $aggregations[$aggregation['name']] = [
            'properties' => [
                'value' => [
                    'type' => 'keyword',
                ],
            ],
        ];
    }
}, $cols);

return [
    '_all' => [
        'enabled' => false,
    ],
    'properties' => [
        'id' => [
            'type' => 'long',
        ],
        'public_id' => [
            'type' => 'keyword',
        ],
        'status' => [
            'type' => 'keyword',
        ],
        'title' => [
            'type' => 'text',
        ],
        'slug' => [
            'type' => 'keyword',
        ],
        'description' => [
            'type' => 'text',
        ],
        'created_at' => [
            'type' => 'date',
            'format' => 'yyyy-MM-dd HH:mm:ss'
        ],
        'updated_at' => [
            'type' => 'date',
            'format' => 'yyyy-MM-dd HH:mm:ss'
        ],
        'published_at' => [
            'type' => 'date',
            'format' => 'yyyy-MM-dd HH:mm:ss'
        ],
        'price' => [
            'properties' => [
                'amount' => [
                    'type' => 'float'
                ],
                'price_note' => [
                    'type' => 'keyword',
                ],
            ],
        ],
        'user' => [
            'properties' => [
                'id' => [
                    'type' => 'long',
                ],
                'contact' => [
                    'properties' => [
                        'id' => [
                            'type' => 'long',
                        ],
                        'fullname' => [
                            'type' => 'keyword',
                        ],
                        'phone' => [
                            'type' => 'keyword',
                        ],
                        'phone_note' => [
                            'type' => 'keyword',
                        ],
                        'email' => [
                            'type' => 'keyword',
                        ],
                    ],
                ],
            ],
        ],
        'category' => [
            'properties' => [
                'id' => [
                    'type' => 'long',
                ],
                'public_id' => [
                    'type' => 'keyword',
                ],
                'name' => [
                    'type' => 'text',
                ],
                'slug' => [
                    'type' => 'keyword',
                ],
                'parent' => [
                    'properties' => [
                        'id' => [
                            'type' => 'long',
                        ],
                        'public_id' => [
                            'type' => 'keyword',
                        ],
                        'name' => [
                            'type' => 'text',
                        ],
                        'slug' => [
                            'type' => 'keyword',
                        ],
                    ],
                ],
                'root' => [
                    'properties' => [
                        'id' => [
                            'type' => 'long',
                        ],
                        'public_id' => [
                            'type' => 'keyword',
                        ],
                        'name' => [
                            'type' => 'text',
                        ],
                        'slug' => [
                            'type' => 'keyword',
                        ]
                    ],
                ],
            ],
        ],
        'location' => [
            'properties' => [
                'city' => [
                    'properties' => [
                        'id' => [
                            'type' => 'long'
                        ],
                        'public_id' => [
                            'type' => 'keyword',
                        ],
                        'name' => [
                            'type' => 'text',
                            'analyzer' => 'french',
                        ],
                        'slug' => [
                            'type' => 'keyword',
                        ],
                    ]
                ],
                'zipcode' => [
                    'type' => 'keyword',
                ],
                'address' => [
                    'type' => 'text',
                ],
                'street' => [
                    'type' => 'text',
                ],
                'geo' => [
                    'type' => 'geo_point'
                ],
            ],
        ],
        'fields' => [
            'type' => 'nested',
            'include_in_parent' => true,
            'dynamic' => false,
            'properties' => $aggregations,
        ],
        'pictures' => [
            'type' => 'text',
            // 'include_in_parent' => true,
            // 'dynamic' => false,
        ],
    ],
];
